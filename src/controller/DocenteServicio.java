/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Model.DocenteModel;
import Pojo.CategoriaDocente;
import Pojo.Docente;
import Pojo.EstadoCivil;
import Pojo.GradoAcademico;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author Jadpa21
 */
public class DocenteServicio {
    private DocenteModel docenteModel;

    public DocenteServicio() {
        docenteModel = new DocenteModel();
    }

    public DocenteServicio(DocenteModel docenteModel) {
        this.docenteModel = docenteModel;
    }
    
    public void saveDocente(Docente docente){
        docenteModel.save(docente);
    }
    
    public void updateDocente(Docente docente){
        docenteModel.update(docente);
    }
    
    //Revisar
    public void saveOrUpdate(Docente docente){        
        Docente tmp = docenteModel.findAny((d) -> d.getCodigo() == docente.getCodigo());
        if(tmp != null){
            updateDocente(docente);
        }else{
            saveDocente(docente);
        }        
    }
    
    
    public List<Docente> getAllDocentes(){
        return docenteModel.getAll();
    }
    
    public ComboBoxModel getCmbModelEstadoCivil(){
        return new DefaultComboBoxModel(EstadoCivil.values());
    }
    
    public ComboBoxModel getCmbModelGradoAcademico(){
        return new DefaultComboBoxModel(GradoAcademico.values());
    }
    
    public ComboBoxModel getCmbModelCategoriaDocente(){
        return new DefaultComboBoxModel(CategoriaDocente.values());
    }
}
