/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Model.DocenteModel;
import Pojo.CategoriaDocente;
import Pojo.Docente;
import Pojo.EstadoCivil;
import Pojo.GradoAcademico;
import Pojo.Sexo;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Jadpa21
 */
public class GestionDocenteController {
    private DocenteModel dmodel;
    //Con header[], establecemos los nombres de cabecera
    private String header[] = {"Codigo", "Cedula", "Inss", "Nombres", "Apellidos", "Fecha Nacimiento", "Sexo", "Estado Civil", "Grado Academico", "Categoria Docente"};

    public GestionDocenteController() {
        dmodel = new DocenteModel();
    }

    public GestionDocenteController(DocenteModel dmodel) {
        this.dmodel = dmodel;
    }

    public DocenteModel getDmodel() {
        return dmodel;
    }

    public String[] getHeader() {
        return header;
    }

    public Object[][] getData() {
        return getData(dmodel.getAll());
    }
    
    public Docente findDocenteByCodigo(int codigo){
        return dmodel.findAny((d)-> d.getCodigo() == codigo);
    }

    public boolean deleteDocente(Docente docente){
        return dmodel.delete(docente);
    }
    
    //Muestra en la tabla los campos ya llenos
    public Object[][] getData(List<Docente> list) {
        Object[][] data = null;

        if (list.isEmpty()) {
            return data;
        }
        
        data = new Object[list.size()][header.length];
        int i = 0;

        for (Docente d : list) {
            data[i] = getDocenteAsArray(d);
            i++;
        }

        return data;
    }

    //Coloca los campos en las filas establecidas
    public Object[] getDocenteAsArray(Docente d) {
        Object[] row = new Object[10];
        row[0] = d.getCodigo();
        row[1] = d.getCedula();
        row[2] = d.getINSS();
        row[3] = d.getNombres();
        row[4] = d.getApellidos();
        row[5] = d.getFecha_de_Nacimiento();
        row[6] = d.getSexo();
        row[7] = d.getEstadocivil();
        row[8] = d.getGradoAcademico();
        row[9] = d.getCategoriaDocente();
        return row;
    }

    //<<<Retorna todos los datos hacia la tabla>>>
    public TableModel getTableModel() {
        return new DefaultTableModel(getData(), header);
    }

    public TableColumnModel setPreferredWidth(TableColumnModel tcmodel, int index[], int width[]) {
        int j = 0;
        for (int i : index) {
            tcmodel.getColumn(i).setPreferredWidth(width[j++]);
        }
        return tcmodel;
    }

    //Recibe como parámetro el elemento seleccionado y el campo de texto a establecer
    public DefaultTableModel filterCase(int indexSelected, String filter) {
        return filterCase(indexSelected, filter, false);
    }

    //Busca en la tabla lo que se le pide
    public DefaultTableModel filterCase(int indexSelected, String filter, boolean auto) {
        Object[][] data = null;
        Docente tmp = null;
        List<Docente> listTmp = null;

        if (filter.equals("")) {
            data = getData();
            return new DefaultTableModel(data, header);
        }

        switch (indexSelected) {
            case 0://codigo            
                int cod = Integer.parseInt(filter);
                if (auto) {
                    listTmp = dmodel.findMany((d) -> String.valueOf(d.getCodigo()).startsWith(filter));
                } else {
                    tmp = dmodel.findAny((d) -> d.getCodigo() == cod);
                }

                break;
            case 1:
                if (auto) {
                    listTmp = dmodel.findMany((d) -> d.getCedula().toLowerCase().startsWith(filter.toLowerCase()));
                } else {
                    tmp = dmodel.findAny((d) -> d.getCedula().equalsIgnoreCase(filter));
                }
                break;
            case 2:
                if (auto) {
                    listTmp = dmodel.findMany((d) -> d.getINSS().toLowerCase().startsWith(filter.toLowerCase()));
                } else {
                    tmp = dmodel.findAny((d) -> d.getINSS().equalsIgnoreCase(filter));
                }
                break;
            case 3:
                if (auto) {
                    listTmp = dmodel.findMany((d) -> d.getApellidos().toLowerCase().startsWith(filter.toLowerCase()));
                } else {
                    listTmp = dmodel.findMany((d) -> d.getApellidos().equalsIgnoreCase(filter));
                }
                break;
            case 4:
                listTmp = dmodel.findMany((d) -> d.getSexo().ordinal() == Sexo.valueOf(filter).ordinal());
                //data = getData(listTmp);
                break;
            case 5:
                listTmp = dmodel.findMany((d) -> d.getEstadocivil().ordinal() == EstadoCivil.valueOf(filter).ordinal());
                //data = getData(listTmp);
                break;
            case 6:
                listTmp = dmodel.findMany((d) -> d.getGradoAcademico().ordinal() == GradoAcademico.valueOf(filter).ordinal());
                //data = getData(listTmp);
                break;
            case 7:
                listTmp = dmodel.findMany((d) -> d.getCategoriaDocente().ordinal() == CategoriaDocente.valueOf(filter).ordinal());
                //data = getData(listTmp);
                break;
        }

        if (listTmp != null) {
            data = getData(listTmp);
        } else if (tmp != null) {
            data = new Object[][]{getDocenteAsArray(tmp)};
        } else {
            data = new Object[][]{null};
        }

        return new DefaultTableModel(data, header);
    }

    //Establece el nombre para ser retornado
    public ComboBoxModel<String> getEnumCmbModel(Enum<?> e) {
        return new DefaultComboBoxModel(enumArrayToStringArray(e.getClass().getEnumConstants()));
    }

    //Busca el nombre que se le pasa como párametro en el Enum
    private String[] enumArrayToStringArray(Enum[] constants) {
        String values[] = new String[constants.length];
        int i = 0;
        for (Enum e : constants) {
            values[i++] = e.name();
        }

        return values;
    }
    
    
}
