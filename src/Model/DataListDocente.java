/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Pojo.Docente;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jadpa16
 */
public class DataListDocente {
    private static List<Docente> dataDocentes;
    
    static{
        dataDocentes = new ArrayList<>();
    }
    
    public static List<Docente> getDataDocentes(){
        return dataDocentes;
    }
}
