package Model;

import Pojo.CategoriaDocente;
import Pojo.Docente;
import Pojo.EstadoCivil;
import Pojo.GradoAcademico;
import Pojo.Sexo;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author _TERIO_
 */
public class DocenteModel {
    private List<Docente> docentes;

    //Manda a llamar al metodo .getDataDocente() que esta en la calse DataListDocente
    public DocenteModel() {
        docentes = DataListDocente.getDataDocentes();        
    }

    public void save(Docente docente) {
        docentes.add(docente);
    }

    public int update(Docente docente) {
        int index = Collections.binarySearch(docentes, docente, (Docente t, Docente t1) -> {
                    if (t.getCodigo() > t1.getCodigo()) {
                        return 1;
                    } else if (t.getCodigo() < t1.getCodigo()) {
                        return -1;
                    } else {
                        return 0;
                    }
                });
        if (index < 0) {
            return -1;
        }
        docentes.set(index, docente);

        return 0;
    }
    
    public boolean delete(Docente docente){
        return docentes.remove(docente);
    }

    public List<Docente> getAll() {
        return docentes;
    }

    public Docente findAny(Predicate<Docente> p) {
        Optional<Docente> opt = docentes.stream().filter(p).findAny();
        return opt.isPresent() ? opt.get() : null;
    }

    public List<Docente> findMany(Predicate<Docente> p) {
        return docentes.stream().filter(p).collect(Collectors.toList());
    }

    public void populate() {
        Docente[] array = {
            new Docente(1, "12/02/1985", "Pepito", "Perez",
            Sexo.Masculino, EstadoCivil.Soltero,
            GradoAcademico.Doctorado, CategoriaDocente.Asistente,
            "001-120285-0045A", "16598-5","pepito.jpg"),
            new Docente(2, "11/05/1984", "Ana", "Conda",
            Sexo.Femenino, EstadoCivil.Soltero,
            GradoAcademico.Maestria, CategoriaDocente.AuxiliarCatedra,
            "001-110584-0001B", "16685-2", "ana.jpg")
        };
        docentes.addAll(Arrays.asList(array));
    }
}
